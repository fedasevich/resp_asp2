﻿using System.Diagnostics;
using System.Net.NetworkInformation;
using System.Text;
using System.Security.Cryptography;

using System.Globalization;

foreach (NetworkInterface ni in NetworkInterface.GetAllNetworkInterfaces())
{
    Console.WriteLine("Name: " + ni.Name);
    Console.WriteLine("Description: " + ni.Description);
    Console.WriteLine("Status: " + ni.OperationalStatus);
    Console.WriteLine("Interface type: " + ni.NetworkInterfaceType);
    Console.WriteLine("Speed: " + ni.Speed);
    Console.WriteLine();
}

Stopwatch stopwatch = new Stopwatch();

stopwatch.Start();

for (int i = 0; i < 1000; i++)
{
    Console.WriteLine(i);
}

stopwatch.Stop();

Console.WriteLine("Execution time: {0}ms", stopwatch.ElapsedMilliseconds);


string data = "Hello, World!";
using (SHA256 sha256Hash = SHA256.Create())
{
    byte[] bytes = sha256Hash.ComputeHash(Encoding.UTF8.GetBytes(data));

    StringBuilder builder = new StringBuilder();
    for (int i = 0; i < bytes.Length; i++)
    {
        builder.Append(bytes[i].ToString("x2"));
    }
    string hash = builder.ToString();

    Console.WriteLine($"Data: {data}");
    Console.WriteLine($"Hash: {hash}");
}

StringComparer comparer = StringComparer.OrdinalIgnoreCase;
Console.WriteLine(comparer.Equals("Hello", "HELLO"));



CultureInfo ci = new CultureInfo("de-DE");

Console.WriteLine(string.Format(ci, "{0:N}", 123456.789));

Console.WriteLine(string.Format(ci, "{0:D}", DateTime.Now));